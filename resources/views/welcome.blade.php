<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!.. Bootstrap CSS ..>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-e0JMYsd53ii+sc0/bJgfsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwa6" crossorigin="anonymous">
        <title>biblioteca</title>

        <!-- Fonts -->
        <!-- Styles -->
        <link href="{{ asset('css/principal.css') }}" rel="stylesheet">
    </head>
    <body>
    <nav>
        <ul style = "text-align:center" class="menu">
            <li class="item"><a href="/">Home</a></li>
            <li class="item"><a href="/biblioteca">Livros</a></li>
            <li class="item"><a href="/autores">Autores</a></li>
            <li class="item"><a href="/editora">Editoras</a></li>
        </ul>
    </nav>
        <div class="flex-center position-ref full-height">
         <h1>Biblioteca-Camile Almeida Gomes Dias</h1>
         <hr>
        </div>
        <a href="/biblioteca"> 
        <button>Biblioteca</button></a>
    </body>
</html>