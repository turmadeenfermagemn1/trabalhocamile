<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-witch, initial-scale=1">

        <!.. Bootstrap CSS ..>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-e0JMYsd53ii+sc0/bJgfsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwa6" crossorigin="anonymous">

        <title>Blibioteca</title>

        <!.. Styles ..>
        <link href="{{ asset('css/principal.css') }}" rel="stylesheet">
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                margin: 5px;
                padding: 5px;
            }
            th, td {
                text-align: center;
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
    <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/">Home</a></li>
                <li class="item"><a href="/biblioteca">Livros</a></li>
                <li class="item"><a href="/autores">Autores</a></li>
                <li class="item"><a href="/editora">Editoras</a></li>
            </ul>
        </nav>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-4">
                    <h2>Listagem de Autores</h2>
                </div>
                <hr>
                <div class="col-md-6">
                <table>
                    <tr>
                        <td>Título</td>
                        <td>Autor</td>
                        <td>Ação</td>
                    </tr>
                    @foreach ($autor as $autor)
                        <tr>
                            <td>{{ $autor->id }}</td>
                            <td>{{ $autor->autor }}</td>
                            <td> <button id="btneditar">Editar</button> <button id="btndeletar">Deletar</button></td>
                        </tr>
                    @endforeach
                </table>
                </div>
            </div>
        </div>
    </body>
</html>