<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-witch, initial-scale=1">
        <title>Biblioteca</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                margin: 5px;
                padding: 5px;
            }
            th, td {
                text-align: center;
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <ul style = "text-align:center" class="menu">
            <li class="item"><a href="/">Home</a></li>
            <li class="item"><a href="/biblioteca">Livros</a></li>
            <li class="item"><a href="/autores">Autores</a></li>
            <li class="item"><a href="/editora">Editoras</a></li>
        </ul>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-4">
                    <h2>Listagem de livros</h2>
                </div>
                <hr>
                <div class="col-md-6">
                <table>
                    <tr>
                        <td>Id</td>
                        <td>Título</td>
                        <td>Autor</td>
                        <td>Editora</td>
                        <td>Ação</td>
                    </tr>
                    @foreach($livro as $livro)
                    <tr>
                        <td class="tab" class="tab" width="500">
                            {{$livro->id}}
                        </td>
                        <td class="tab" width="500">
                            {{$livro->livro}}
                        </td>
                        <td  class="tab" width="500">                               
                            {{$livro->id_autor}}
                        </td>
                        <td class="tab" width="500">
                            {{$livro->id_editora}}
                        </td>
                        </td>
                        <td class="tab" width="500">
                            <button id="btneditar">Editar</button> <button id="btndeletar">Deletar</button>
                        </td>
                    </tr>
                    @endforeach
                </table>
                </div>
            </div>
        </div>
    </body>
</html>