<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Biblioteca;

Route::get('/',function() 
{ 
    return view ('welcome'); 
});

Route::get('/biblioteca', [Biblioteca::class, 'retorno']);

Route::get('/autores', [Biblioteca::class, 'autor']); 

Route::get('/editora', [Biblioteca::class, 'editora']); 

Route::get('/editarautores', [Biblioteca::class, 'editautor']);

Route::get('/editarlivros', [Biblioteca::class, 'editlivro']);

Route::get('/editareditoras', [Biblioteca::class, 'editeditora']);